function getYear() {
    const date = new Date().getFullYear()
    return date
}
const en = {
    'title': 'basliq',
    'copyright': `All rights reserved © 2020-${getYear()}`,
    'makeYourIdeasAlive': 'Make your ideas <span style="font-style: italic; font-weight: 800">alive</span>',
    'communityForTalentedEntrepreneurs': 'Community for talented entrepreneurs',
    'weOfferOurSkillsAndExperiences': 'We, <span>Ideas.Foundation</span>, offer our own skills and experience to implement your ideas',
    'joinedUs': 'Joined us::',
    'IWantToJoin': 'I want to join',
    'allRightsReserved': 'All rights reserved',
    'about': 'About',
    'myIdeaAsGoodAsSliconValleyIdeas': 'My idea is as good as ideas created in <span style="font-style: italic; font-size: 16px; font-weight: 800">Silicon Valley</span>',
    'fullName': 'Full name',
    'youCanWriteUsAboutYourProjectsOrYouself': 'You can write us about your projects or youself',
    'amTalent': 'I am a talent!',
    'yourMessageIsTravellingThroughTheSpace': 'Your message is travelling through the space',
    'thankYou': 'Thank you',
}

export default en

