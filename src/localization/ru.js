function getYear() {
    const date = new Date().getFullYear()
    return date
}
const ru = {
    'title': 'basliq',
    'copyright': `Все права защищены © 2020-${getYear()}`,
    'makeYourIdeasAlive': 'Оживите свои <span style="font-style: italic; font-weight: 800">идеи</span>',
    'communityForTalentedEntrepreneurs': 'Сообщество для талантливых предпринимателей',
    'weOfferOurSkillsAndExperiences': 'Мы, <span>Ideas.Foundation</span>, предлагаем наши навыки и опыт для реализации ваших идей',
    'joinedUs': 'Наши участники:',
    'IWantToJoin': 'Хочу к вам',
    'about': 'О нас',
    'myIdeaAsGoodAsSliconValleyIdeas': 'Мои идеи не хуже идей, созданных в Кремниевой долине',
    'fullName': 'Имя Фамилия',
    'youCanWriteUsAboutYourProjectsOrYouself': 'Можете написать о ваших проектах или о себе',
    'amTalent': 'Я талант!',
    'yourMessageIsTravellingThroughTheSpace': 'Ваше обращение путешествует по космосу',
    'thankYou': 'Спасибо',
}

export default ru